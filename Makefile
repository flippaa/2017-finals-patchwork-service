.PHONY: all test install clean install-deps strip

CPP_FLAGS=-Wno-unused-result -std=c++11
LSB=src/ConsecutiveLSB.cpp src/PrngLSB.cpp


all:
	mkdir -p build
	g++ $(CPP_FLAGS) -o build/extractor $(LSB) src/extractor.cpp
	g++ $(CPP_FLAGS) -o build/embeddor $(LSB) src/embeddor.cpp


test:
	python test/test_embedding.py build/embeddor build/extractor 1
	python test/test_embedding.py build/embeddor build/extractor 2


strip:
	strip -s build/extractor


install:
	scripts/create_db.sh || :
	sudo mkdir -p /opt/patchwork
	sudo chmod -R 0777 /opt/patchwork
	sudo cp build/extractor /opt/patchwork || :
	sudo cp service.py /opt/patchwork || :


clean:
	rm -rf build


install-deps:
	sudo apt-get install postgresql
	pip install -r requirements.txt
