#!/usr/bin/env bash

password=changeme
sudo -i -u postgres psql -c "CREATE DATABASE ServiceDb ;"
sudo -i -u postgres psql servicedb -c "CREATE TABLE capsules(id SERIAL PRIMARY KEY, timestamp TIMESTAMP, label TEXT, capsule TEXT);"
sudo -i -u postgres psql -c "CREATE USER oracle WITH PASSWORD '$password' ;"
sudo -i -u postgres psql servicedb -c "ALTER TABLE capsules OWNER TO oracle ;"
sudo -i -u postgres psql servicedb -c "ALTER DATABASE serviceDb OWNER TO oracle ;"

