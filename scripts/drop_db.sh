#!/usr/bin/env bash

echo "Drop all data from db ServiceDb..."
sudo -i -u postgres psql servicedb -c "DROP TABLE capsules ;"
sudo -i -u postgres psql -c "DROP DATABASE servicedb ;"
sudo -i -u postgres psql -c "DROP  USER oracle ;"

