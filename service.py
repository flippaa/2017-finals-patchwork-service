# !/usr/bin/python
# -*- coding: utf-8 -*-
import SocketServer
import umsgpack
import struct
import sys
import random
import logging
import psycopg2
import subprocess


from datetime import datetime
from string import ascii_letters, digits


ADDRESS = '0.0.0.0'
PORT = 9091


class IOMsgException(Exception):
    pass


def get_random_name():
    return ''.join(random.choice(ascii_letters + digits) for _ in range(random.randint(5, 10)))


def add_capsule(label, capsule):
    conn = psycopg2.connect(dbname="servicedb", host="localhost", user="oracle", password="changeme", port=5432)
    with conn:
        cur = conn.cursor()
        cur.execute("INSERT INTO capsules(label, timestamp, capsule) "
                    "VALUES((%s), (%s), (%s))", [label, datetime.now(), capsule])
    conn.close()
    return 0


def get_capsule(label):
    conn = psycopg2.connect(dbname="servicedb", host="localhost", user="oracle", password="changeme", port=5432)
    with conn:
        cur = conn.cursor()
        cur.execute("SELECT capsule FROM capsules WHERE label=(%s)", [label])
        capsule = cur.fetchone()
    logger.debug("Capsule from db by label {}".format(label))

    if capsule:
        logger.debug("Capsule={}".format(capsule[0]))
        return capsule[0]

    conn.close()
    return ''


def pack_message(msg):
    return umsgpack.dumps(msg)


def pack_img(img):
    return umsgpack.dumps(img)


def unpack_message(packed_msg):
    return umsgpack.loads(packed_msg)


def read_message(s):
    received_buffer = s.recv(4)
    if len(received_buffer) < 4:
        raise IOMsgException('Failed to read length of a message')
    to_receive = struct.unpack('>I', received_buffer[0:4])[0]
    received_buffer = ''
    while len(received_buffer) < to_receive:
        buf = s.recv(to_receive - len(received_buffer))
        if len(buf) == 0:
            raise IOMsgException('Failed to read data from socket: the pipe must have been broken')
        received_buffer += buf
    return received_buffer


def send_message(s, msg):
    send_buffer = struct.pack('>I', len(msg)) + msg
    s.sendall(send_buffer)


class ForkingTCPServer(SocketServer.ForkingMixIn, SocketServer.TCPServer):
    pass


class ServiceHandler(SocketServer.BaseRequestHandler):
    def __init__(self, request, client_address, server):
        SocketServer.BaseRequestHandler.__init__(self, request, client_address, server)

    def handle(self):
        logger.info('Accepted connection from {0}'.format(self.client_address[0]))
        try:
            self.request.settimeout(30)

            packed_msg = read_message(self.request)
            logger.info('Accepted message from {0}'.format(self.client_address[0]))

            unpacked_msg = unpack_message(packed_msg)

            if unpacked_msg['cmd'] == 'push':

                send_message(self.request, pack_message(1))

                tmp_name = get_random_name()
                with open(tmp_name, "wb") as f:
                    f.write(unpacked_msg['img'])

                extracted = ''
                try:
                    extracted = subprocess.check_output(["./extractor", tmp_name, str(unpacked_msg['method'])])
                except subprocess.CalledProcessError as ex:
                    logger.error("Extractor returned a non-zero status")

                logger.info(extracted)
                add_capsule(unpacked_msg['label'], extracted)

            elif unpacked_msg['cmd'] == 'pull':

                extracted = get_capsule(unpacked_msg['label'])
                packed_msg = pack_message({'capsule': extracted})
                send_message(self.request, packed_msg)

            else:

                logger.error('Unknown command {0} from {1}'.format(unpacked_msg['cmd'], self.client_address[0]))
                send_message(self.request, pack_message(0))

        except ImportError as ex:
            logger.error(ex)
        except psycopg2.IntegrityError as ex:
            logger.error('Db auth error')
        except Exception as ex:
            logger.error(str(ex), exc_info=True)
        finally:
            logger.info('Processed connection from {0}'.format(self.client_address[0]))
        return


if __name__ == '__main__':

    if len(sys.argv) > 1:
        ADDRESS = sys.argv[1]
    if len(sys.argv) > 2:
        PORT = int(sys.argv[2], 9091)
    logger = logging.getLogger(__name__)
    logging.basicConfig(format='[%(asctime)s] %(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S',
                        level=logging.DEBUG)
    address = (ADDRESS, PORT)
    server = ForkingTCPServer(address, ServiceHandler)
    server.serve_forever()
