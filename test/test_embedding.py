# -*- coding: utf-8 -*-
import os
import tempfile
import contextlib
import random
import argparse
import subprocess
from PIL import Image


@contextlib.contextmanager
def temp_file():
    file_path = '{0}.bmp'.format(tempfile.mkstemp()[1])
    try:
        yield file_path
    finally:
        os.remove(file_path)


if __name__ == '__main__':
    # parse cmd args
    parser = argparse.ArgumentParser()
    parser.add_argument('embeddor_path', type=str, help='Path to embeddor binary.')
    parser.add_argument('extractor_path', type=str, help='Path to extractor binary.')
    parser.add_argument('alg_code', type=int, help='Algorithm code.')
    parser.add_argument('--n_iter', type=int, default=100, help='Number of test rounds.')
    args = parser.parse_args()

    for i in range(args.n_iter):
        with temp_file() as cover_image_path:
            with temp_file() as stego_image_path:
                # generate a cover image and save it
                print('Test {0}/{1}'.format(i + 1, args.n_iter))
                size = (random.randint(1000, 1601), random.randint(1500, 2501))
                image = Image.new('RGB', size, color=0)
                image.save(cover_image_path)

                # generate parameters
                flag = 'VolgaCTF{{{0}}}'.format(''.join([random.choice('0123456789ABCDEF') for _ in range(32)]))
                print('    embedding flag {0} with parameters {1}'.format(flag, args.alg_code))

                # embed
                try:
                    cmd = '{0} {1} {2} {3} {4}'.format(args.embeddor_path, cover_image_path,
                                                       args.alg_code, flag, stego_image_path)
                    print('    executing {0}'.format(cmd))
                    subprocess.call(cmd, shell=True)
                except Exception as ex:
                    print('    Failed to embed flag: {0}'.format(ex))
                    continue

                # extract
                try:
                    cmd = '{0} {1} {2}'.format(args.extractor_path, stego_image_path, args.alg_code)
                    print('    executing {0}'.format(cmd))
                    output = (subprocess.check_output(cmd, shell=True)).decode()
                except subprocess.CalledProcessError as e:
                    output = (e.output).decode()
                print('    extracted flag {0}'.format(output))

                # compare
                if output == flag:
                    print('    the embedded and retrieved flags match')
                else:
                    print('    the embedded and retrieved flags don\'t match')
                    raise Exception('Test failed!!!')
                print('')

    print('Tested {0} embeddings.'.format(args.n_iter))
