# Patchwork-service #

Service for VolgaCTF-2017. Service extracts capsule from bmp images, participants must generate bmp image, hide capsule with rop shellcode in image and place pointer to rop beginning into the image header.

### For testing ###

* `sudo apt $(deb_requirements.txt)`
* `./venv_creator.sh`
* `source .venv/bin/activate`
* `pip install -r requirements.txt`
* `./create_db.sh`
* `python service.py`

For test run:
* `./extractor outfile 1`

### For gaming ###

* `sudo apt $(deb_requirements.txt)`
* `pip install -r requirements.txt`
* `./create_db.sh`
* `python service.py`

Remove all but `service.py` and `changeme`
