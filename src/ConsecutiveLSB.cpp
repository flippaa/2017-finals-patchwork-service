#include "ConsecutiveLSB.h"
#include <cstring>


void ConsecutiveLSB::embed(uint8_t* data, int width, int height, uint8_t* stego, int length)
{
    // embed the length of the stego message
    int a = length;
    for (int i = 0; i < 32; ++i)
    {
        data[3*i] = (data[3*i] & 0xfe) | (a & 1);
        a >>= 1;
    }

    // embed the stego message
    for (int i = 0; i < length; ++i)
    {
        uint8_t b = stego[i];
        for (int j = 0; j < 8; ++j)
        {
            data[3*(8*i + j + 32)] = (data[3*(8*i + j + 32)] & 0xfe) | (b & 1);
            b >>= 1;
        }
    }
}


uint8_t* ConsecutiveLSB::extract(uint8_t* data, int width, int height)
{
    // read the length of the embedded stego message
    int length = 0;
    for (int i = 0; i < 32; ++i)
    {
        length |= (data[3*i] & 1) << i;
    }

    // allocate the buffer
    uint8_t* stego = new uint8_t[length];
    std::memset(stego, 0, sizeof(uint8_t) * length);

    // extract the stego message
    for (int i = 0; i < length; ++i)
    {
        for (int j = 0; j < 8; ++j)
            stego[i] |= (data[3*(8*i + j + 32)] & 1) << j;
    }

    return stego;
}
