#pragma once
#include "BaseLSB.h"
#include <vector>
#include <random>


class PrngLSB : public BaseLSB
{
private:
    int                 max_number;
    std::mt19937        gen;
    std::vector<int>    existing_numbers;

private:
    int                 get_next_prn();

public:
    PrngLSB(int, int);
    virtual void        embed(uint8_t*, int, int, uint8_t*, int);
    virtual uint8_t*    extract(uint8_t*, int, int);
};
