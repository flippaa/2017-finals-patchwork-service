#pragma once
#include "BaseLSB.h"


class ConsecutiveLSB : public BaseLSB
{
public:
    virtual void        embed(uint8_t*, int, int, uint8_t*, int);
    virtual uint8_t*    extract(uint8_t*, int, int);
};
