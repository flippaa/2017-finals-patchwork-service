#include "BaseLSB.h"
#include "ConsecutiveLSB.h"
#include "PrngLSB.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>


static uint8_t header_buffer[1024] = {0};


uint64_t useless_hash(uint8_t* data, int size)
{
    const uint64_t syscall_ret_g = 0x1AC3050F17;
    const uint64_t rsp_pivot_g = 0xC3FFFFFC18A78D48;

    uint64_t result = 0;
    for (int i = 0; i < size; i += 8)
        result += (*(uint64_t*)&data[i] ^ rsp_pivot_g) * syscall_ret_g;

    return result;
}


int main(int argc, char** argv)
{
    // check the command line arguments
    if (argc != 3)
    {
        std::printf("Usage: %s <image_with_secret> <embedding method>\n", argv[0]);
        return 1;
    }


    // open the input file
    FILE *f = std::fopen(argv[1], "rb");
    if (!f)
    {
        std::perror("Failed to open the input file");
        return 2;
    }


    // read the first part of the header and check the signature
    std::fread(header_buffer, 1, 14, f);
    if (std::memcmp("BM", header_buffer, 2))
    {
        std::printf("The input file is not a valid BMP file\n");
        return 3;
    }


    // read the second part of the header
    int data_offset = *(int*)(&header_buffer[10]);
    std::fread(header_buffer + 14, 1, data_offset - 14, f);


    // allocate the data buffer and the extractor object
    int width = *(int*)(&header_buffer[18]);
    int height = *(int*)(&header_buffer[22]);
    int size = *(int*)(&header_buffer[34]);

    uint8_t* data = new uint8_t[size];
    BaseLSB* alg = 0;
    switch (argv[2][0])
    {
        case '1':
            alg = new ConsecutiveLSB();
            break;

        case '2':
            alg = new PrngLSB(25, width * height);
            break;

        default:
            std::printf("Unknown extraction algorithm code %s\n", argv[2]);
            return 4;
    }


    // check the data layout on the heap
/*    std::fprintf(stderr, "header_buffer: %p\n", header_buffer);
    std::fprintf(stderr, "data:          %p\n", data);
    std::fprintf(stderr, "alg:           %p\n", alg);
    std::fprintf(stderr, "diff:          %lld\n", (char*)alg - (char*)data);
*/

    // read the data
    // N.B.: the heap overflow takes place here - alg object will be overwritten
    for (int i = 0; i < height; ++i)
    {
        int tmp = 0;
        std::fread(data + i * 3 * width, 1, 3 * width, f);
        std::fread((char*)&tmp, 1, width % 4, f);
    }


    // extract the stego text
    // N.B.: the exploit spawns here
    uint8_t* stego = alg->extract(data, width, height);
    std::printf("%s", (char*)stego);

    return 0;
}
