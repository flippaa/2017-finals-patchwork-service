#include "PrngLSB.h"
#include <algorithm>
#include <cstring>


PrngLSB::PrngLSB(int seed, int max_number)
{
    this->max_number = max_number - 1;
    this->gen.seed(seed);
}


int PrngLSB::get_next_prn()
{
    std::uniform_int_distribution<int> uid(0, this->max_number);
    while (1)
    {
        int new_int = uid(this->gen);
        if (std::find(this->existing_numbers.begin(), this->existing_numbers.end(), new_int) != this->existing_numbers.end())
        {
            continue;
        }
        else
        {
            this->existing_numbers.push_back(new_int);
            return new_int;
        }
    }
}


void PrngLSB::embed(uint8_t* data, int width, int height, uint8_t* stego, int length)
{
    // embed the length of the stego message
    uint32_t a = length;
    for (int i = 0; i < 32; ++i)
    {
        int prng_index = this->get_next_prn();
        data[3*prng_index] = (data[3*prng_index] & 0xfe) | (a & 1);
        a >>= 1;
    }

    // embed the stego message
    for (int i = 0; i < length; ++i)
    {
        uint8_t b = stego[i];
        for (int j = 0; j < 8; ++j)
        {
            int prng_index = this->get_next_prn();
            data[3*prng_index] = (data[3*prng_index] & 0xfe) | (b & 1);
            b >>= 1;
        }
    }
}


uint8_t* PrngLSB::extract(uint8_t* data, int width, int height)
{
    // read the length of the embedded stego message
    int length = 0;
    for (int i = 0; i < 32; ++i)
    {
        int prng_index = this->get_next_prn();
        length |= (data[3*prng_index] & 1) << i;
    }

    // allocate the buffer
    uint8_t* stego = new uint8_t[length];
    std::memset(stego, 0, sizeof(uint8_t) * length);

    // extract the stego message
    for (int i = 0; i < length; ++i)
    {
        for (int j = 0; j < 8; ++j)
        {
            int prng_index = this->get_next_prn();
            stego[i] |= (data[3*prng_index] & 1) << j;
        }
    }

    return stego;
}
