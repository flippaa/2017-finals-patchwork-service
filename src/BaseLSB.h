#pragma once
#include <stdint.h>


class BaseLSB
{
public:
    virtual void        embed(uint8_t*, int, int, uint8_t*, int) = 0;
    virtual uint8_t*    extract(uint8_t*, int, int) = 0;
};
