#include "BaseLSB.h"
#include "ConsecutiveLSB.h"
#include "PrngLSB.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>


static uint8_t header_buffer[1024] = {0};


int main(int argc, char** argv)
{
    if (argc != 5)
    {
        std::printf("Usage: %s <input image> <embedding method> <flag> <output file>\n", argv[0]);
        return 1;
    }


    // open the input file
    FILE *f = std::fopen(argv[1], "rb");
    if (!f)
    {
        std::perror("Failed to open the input file");
        return 2;
    }


    // read the first part of the header and check the signature
    // read the second part of the header
    std::fread(header_buffer, 1, 14, f);
    int data_offset = *(int*)(&header_buffer[10]);
    std::fread(header_buffer + 14, 1, data_offset - 14, f);


    // allocate the data buffer and read the data
    int width = *(int*)(&header_buffer[18]);
    int height = *(int*)(&header_buffer[22]);
    int size = *(int*)(&header_buffer[34]);
    uint8_t* data = new uint8_t[size];

    for (int i = 0; i < height; ++i)
    {
        int tmp = 0;
        std::fread(data + i * 3 * width, 1, 3 * width, f);
        std::fread((char*)&tmp, 1, width % 4, f);
    }


    // allocate the extractor object
    BaseLSB* alg = 0;
    switch (argv[2][0])
    {
        case '1':
            alg = new ConsecutiveLSB();
            break;

        case '2':
            alg = new PrngLSB(25, width * height);
            break;

        default:
            std::printf("Unknown extraction algorithm code %s\n", argv[2]);
            return 4;
    }


    // embed the stego text
    char* capsule = argv[3];
    alg->embed(data, width, height, (uint8_t*)capsule, std::strlen(capsule));


    // save the result of embedding to a given file
    FILE *fout = std::fopen(argv[4], "wb+");
    if (!fout)
    {
        std::perror("Failed to open the output file");
        return 5;
    }

    std::fwrite(header_buffer, 1, 14, fout);
    std::fwrite(header_buffer + 14, 1, data_offset - 14, fout);

    for (int i = 0; i < height; ++i)
    {
        int tmp = 0;
        std::fwrite(data + i * 3 * width, 1, 3 * width, fout);
        std::fwrite((char*)&tmp, 1, width % 4, fout);
    }

    return 0;
}